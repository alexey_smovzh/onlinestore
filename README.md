Pet project. 
Online Store written on Flask.


Used software versions:
```
    - Python 3.7
    - Flask 2.0.1
```


Application consist from two modules:
- onlinestore - Online Store itself, client side
- admin - Admin Panel


Init database from Admin Application

```
    FLASK_APP=lib flask init-db
```

Run app
```
    FLASK_APP=admin flask run

    # or
    FLASK_APP=onlinestore flask run
```