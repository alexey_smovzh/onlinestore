import os
import errno


from flask import Flask
from flask import current_app
from flask import redirect
from flask import url_for


from admin import ingredient
from admin import product
from admin import composition
from admin import unit
from lib import media


DATA=os.path.join("data")


def create_app(test_config=None):
    app = Flask(__name__)
    app.config.update(
        # Data folder
        DATA=DATA,
        # Sqlite3 database file location
        DATABASE=os.path.join(DATA, "database.db"),
        # Image upload path
        MEDIA=os.path.join(DATA, "media"),
    )


    # ensure the data folder exists
    try:
        os.makedirs(app.config["DATA"])       
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # ensure the media folder exists
    try:
        os.makedirs(app.config["MEDIA"])
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


    # Initialize database
    from lib import database
    database.init_app(app)


    # Register route handlers
    app.register_blueprint(ingredient.bp)
    app.register_blueprint(product.bp)
    app.register_blueprint(composition.bp)
    app.register_blueprint(media.bp)
    app.register_blueprint(unit.bp)


    # Default route
    @app.route("/")
    def index():
        return redirect(url_for("product.all"))


    return app
