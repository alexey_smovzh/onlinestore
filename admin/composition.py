import re

from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from lib.database import query_db
from lib.database import select_query_db


bp = Blueprint("composition", __name__, url_prefix="/composition")


@bp.route("/all/<int:id>")
def all(id):
    """ Display composition for product by ID """
    status, composition = select_query_db("SELECT composition.id, ingredient.name, composition.qty \
                                           FROM ingredient, composition \
                                           WHERE ingredient.id = composition.ingredient_id \
                                           AND composition.product_id = ?", [id], fetchall=True)
    if status is not True:
        return render_template("error.html", error=result)

    status, ingredients = select_query_db("SELECT id, name FROM ingredient", fetchall=True)
    if status is not True:
        return render_template("error.html", error=result)
    
    return render_template("composition.html", composition=composition, ingredients=ingredients, product_id=[id])



@bp.route("/add/<int:product_id>", methods=("POST",))
def add(product_id):
    """ Add new ingredient in composition list """
    error = None

    ingredient_id = request.form["ingredient_id"]
    qty = request.form["qty"]

    check = re.search("[0-9]+[а-я]+$", qty)
    if check is None:
        error = "Qty can contain only digits and cyrrilic letters in low case e.q. '20г', '1шт'"
    else:
        result = query_db("INSERT INTO composition(product_id, ingredient_id, qty) VALUES(?,?,?)", [product_id, ingredient_id, qty])

        if result is None:
            return redirect(url_for("composition.all", id=product_id))
    
    return render_template("error.html", error=error)



@bp.route("/delete/<int:product_id>/<int:id>", methods=("POST",))    
def delete(product_id, id):
    """ Delete composition ingredient """
    result = query_db("DELETE FROM composition WHERE id = ?", [id])
    if result is not None:
        return render_template("error.html", error = result)

    return redirect(url_for("composition.all", id=[product_id]))
