import re

from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from lib.database import query_db
from lib.database import select_query_db



bp = Blueprint("ingredient", __name__, url_prefix="/ingredient")


@bp.route("/all")
def all():
    """ Display all ingredients list """
    status, result = select_query_db('SELECT id, name FROM ingredient', fetchall=True)

    if status is True:
        return render_template('ingredient.html', ingredients=result)
    else:
        return render_template('error.html', error=result)



def update(request, id=None):
    """ Update ingredient
        If ID is None - there is INSERT query otherwise UPDATE 
        Return result is ERROR on failure and NONE on success """
    error = None
    query = None

    ingredient = request.form["ingredient"]

    result = re.search("[А-Яа-я ]+$", ingredient)
    if result is None:
        error = "Ingredient name should consist only from cyrrilic letters"
    else:
        if id is None:
            query = query_db('INSERT INTO ingredient(name) VALUES(?)', [ingredient])
        else:
            query = query_db('UPDATE ingredient SET name=? WHERE id=?', [ingredient, id])

    if query is not None:
        error = query

    return error



@bp.route("/edit/<int:id>", methods=("GET", "POST"))
def edit(id):
    """ Edit ingredient name """
    result = None

    if request.method == "POST":        
        result = update(request, id)

        if result is None:
            return redirect(url_for("ingredient.all"))
    
    else:    
        """ Display edit ingredient page """
        status, result = select_query_db('SELECT id, name FROM ingredient WHERE id=?', [id])

        if status is True:
            return render_template('ingredient_edit.html', ingredient=result)

    return render_template('error.html', error=result)    



@bp.route("/add", methods=("GET", "POST"))
def add():
    """ Add new ingredient """
    result = None

    if request.method == "POST":
        result = update(request)

        if result is None:
            return redirect(url_for("ingredient.all"))

    return render_template('error.html', error=result)



@bp.route("/delete/<int:id>", methods=("POST",))
def delete(id):
    """ Delete ingredient by ID """
    error = None

    query = query_db('DELETE FROM ingredient WHERE id = ?', [id])
    if query is not None:
        error = query
    
    if error is None:
        return redirect(url_for("ingredient.all"))

    return render_template('error.html', error=error)
