import os
import re
import imghdr

from flask import current_app
from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from lib.database import query_db
from lib.database import select_query_db


bp = Blueprint("product", __name__, url_prefix="/product")



@bp.route("/all")
def all():
    """ Display all product list """
    status, product = select_query_db("SELECT product.id, product.name, product.qty, product.price, unit.name as unit \
                                       FROM product, unit \
                                       WHERE product.unit_id = unit.id", fetchall=True)
    if status is False:
        return render_template("error.html", error=product)

    status, unit = select_query_db("SELECT id, name FROM unit", fetchall=True)
    if status is False:
        return render_template("error.html", error=unit)

    return render_template("product.html", products=product, units=unit)

    


def update(request, id=None):
    """ Update product name and quantity 
        If ID is None - this is INSERT query otherwise UPDATE existing record by ID 
        Function return result is ERROR on failure and NONE on success """
    error = None
    result = None

    product = request.form["product"]
    qty = request.form["qty"]
    price = request.form["price"]
    unit_id = request.form["unit_id"]

    check = re.search("[А-Яа-я ]+$", product)
    if check is None:
        error = "Product name should consist only from cyrrilic letters"
    else:
        if id is None:
            result = query_db("INSERT INTO product(name, qty, price, unit_id) VALUES(?, ?, ?, ?)", [product, qty, price, unit_id])
        else:
            result = query_db("UPDATE product SET name=?, qty=?, price=?, unit_id=? WHERE id=?", [product, qty, price, unit_id, id])

    if result is not None:
        error = result

    return error


@bp.route("/add", methods=("POST",))
def add():
    """ Add new product, its quantity, price and measurement unit """
    result = update(request)

    if result is None:
        return redirect(url_for("product.all"))

    return render_template("error.html", error=result)



@bp.route("/edit/<int:id>", methods=("POST",))    
def edit_post(id):
    """ Edit product name, quantity, price and measurement unit """
    result = update(request, id)

    if result is None:
        return redirect(url_for("product.all"))



@bp.route("/edit/<int:id>")    
def edit_get(id):
    """ Display edit product name, quantity, price and measurement unit page """
    status, product = select_query_db("SELECT product.id, product.name, product.qty, product.price, unit.id as unit_id \
                                       FROM product, unit \
                                       WHERE product.unit_id = unit.id \
                                       AND product.id = ?", [id])
    if status is False:
        return render_template("error.html", error=product)

    status, unit = select_query_db("SELECT id, name FROM unit", fetchall=True)
    if status is False:
        return render_template("error.html", error=unit)

    return render_template("product_edit.html", product=product, units=unit)



def delete_img(filename):
    """ Delete product image from data/media folder by image name """
    try:
        path = os.path.join(current_app.root_path, "..", current_app.config["MEDIA"], filename)
        os.remove(path)
    except OSError as error:
        return error

    return True



@bp.route("/delete/<int:id>", methods=("POST",))    
def delete(id):    
    """ Delete product and its uploaded image files by ID """
    result = query_db("DELETE FROM product WHERE id = ?", [id])
    if result is not None:
        return render_template("error.html", error = result)

    result = query_db("DELETE FROM composition WHERE product_id = ?", [id])
    if result is not None:
        return render_template("error.html", error = result)

    # delete product image files
    status, result = select_query_db("SELECT name FROM media WHERE product_id = ?", [id], fetchall=True)
    if status is True:
        for image in result:
            result = delete_img(image["name"])
            if result is not True:
                return render_template("error.html", error = result)    
        
        # and delete records from database
        result = query_db("DELETE FROM media WHERE product_id = ?", [id])
        if result is not None:
            return render_template("error.html", error = result)    

    return redirect(url_for("product.all"))

    
    
def validate_image(stream):
    """ Read file header and validate that it has proper PNG format """
    header = stream.read(512)
    stream.seek(0)
    format = imghdr.what(None, header)

    if format is not 'png':
        return False

    return True



@bp.route("/description/<int:id>")
def description(id):
    """ Displays description edit page """
    status, product = select_query_db("SELECT id, name, qty, laconic, full \
                                       FROM product \
                                       WHERE id = ?", [id])
    if status is False:
        return render_template("error.html", error=product)

    status, media = select_query_db("SELECT name FROM media WHERE type='p' AND product_id=?", [id], fetchall=True)
    if status is False:
        return render_template("error.html", error=media)

    # jinja can't access to List element by index
    # convert List into Array
    arr = [None] * 4
    idx = 0
    try:
        for row in media:
            arr[idx] = row['name']
            idx += 1
    except IndexError:
        return render_template("error.html", error="Select from table 'media' return more than 4 rows")

    return render_template("product_description.html", product=product, media=arr)



@bp.route("/description_update/<int:id>", methods=("POST",))
def description_update(id):
    """ Save product images and descriptions """
    idx = 0

    laconic = request.form["laconic"]
    full = request.form["full"]
    
    # update descriptions
    result = query_db("UPDATE product SET laconic=?, full=? WHERE id=?", [laconic, full, id])
    if result is not None:
        return render_template("error.html", error=result)

    # Handle product images
    for img in request.files.getlist("images"):
        if img.filename is not '':
            result = validate_image(img.stream)
            if result is False:
                return render_template("error.html", error="Only files in PNG format are allowed")

            # img_name = <product id> + <image index>.png
            img_name = "%i_%i.png" % (id, idx)
            img.save(os.path.join(current_app.config["MEDIA"], img_name))

            # its faster to serach image name in database instead of on filesystem
            result = query_db("INSERT INTO media(product_id, name, type) VALUES(?, ?, ?)", [id, img_name, "p"])
            if result is not None:
                return render_template("error.html", error=result)

            idx += 1

    return redirect(url_for("product.all"))
