import re

from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from lib.database import query_db
from lib.database import select_query_db


bp = Blueprint("unit", __name__, url_prefix="/unit")


@bp.route("/all")
def all():
    """ Display all measurement units """
    status, result = select_query_db("SELECT id, name FROM unit", fetchall=True)
    if status is not True:
        return render_template("error.html", error=result)
    
    return render_template("unit.html", unit=result)



@bp.route("/add", methods=("POST",))
def add():
    """ Add new measurement unit """
    error = None

    unit = request.form["unit"]

    check = re.search("[0-9]+[а-я]+$", unit)
    if check is None:
        error = "Measurement unit can contain only digits and cyrrilic letters in low case e.q. '1кг', '1шт'"
    else:
        result = query_db("INSERT INTO unit(name) VALUES(?)", [unit])

        if result is None:
            return redirect(url_for("unit.all"))
    
    return render_template("error.html", error=error)



@bp.route("/delete/<int:id>", methods=("POST",))    
def delete(id):
    """ Delete measurement unit by ID """
    result = query_db("DELETE FROM unit WHERE id=?", [id])
    if result is not None:
        return render_template("error.html", error = result)

    return redirect(url_for("unit.all"))
