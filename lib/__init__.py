import os
import errno

from flask import Flask


DATA=os.path.join("data")


def create_app(test_config=None):
    app = Flask(__name__)
    app.config.update(
        # Data folder
        DATA=DATA,
        # Sqlite3 database file location
        DATABASE=os.path.join(DATA, "database.db"),
        # Image upload path
        MEDIA=os.path.join(DATA, "media"),
    )


    # ensure the data folder exists
    try:
        os.makedirs(app.config["DATA"])       
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # ensure the media folder exists
    try:
        os.makedirs(app.config["MEDIA"])
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


    # Initialize database
    from lib import database
    database.init_app(app)

    return app
