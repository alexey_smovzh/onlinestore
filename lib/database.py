import sqlite3

import click

from flask import current_app
from flask import g
from flask.cli import with_appcontext



def get_db_connection():
    """ Create database connection and add it to store """

    if "database" not in g:
        g.database = sqlite3.connect(
            current_app.config["DATABASE"],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        # Enable foreign key constraint support it must be enabled per connection
        g.database.execute('PRAGMA foreign_keys=ON')
        g.database.row_factory = sqlite3.Row

    return g.database



def close_db_connection(e=None):
    """ Close database connection and delete it from store"""
    database = g.pop("database", None)

    if database is not None:
        database.close()


def select_query_db(query, parameters=None, fetchall=None):
    """ Perform SELECT SQL query and return result or error message.
        As first argument return False or True depending if query was successfull """
    database = get_db_connection()
    result = None

    try:
        if fetchall is True:
            if parameters is None:
                result = database.execute(query).fetchall()
            else:
                result = database.execute(query, parameters).fetchall()
        else:
            if parameters is None:
                result = database.execute(query).fetchone()
            else:
                result = database.execute(query, parameters).fetchone()

    except sqlite3.Error as error:
        return False, error

    if result is None:
        return False, "Query return empty result"

    return True, result


def query_db(query, values):
    """ Perform SQL query and return None result on success
        or error message on failure """
    database = get_db_connection()

    try:
        database.execute(query, values)
    except sqlite3.Error as error:
        return error

    database.commit()
    
    return None


@click.command("init-db")
@with_appcontext
def init_db():
    """ Initialize database if not already exists """
    
    database = get_db_connection()        

    with current_app.open_resource("schema.sql") as f:
        database.executescript(f.read().decode("utf8"))



def init_app(app):
    """ Register database functions with Flask app """
    app.teardown_appcontext(close_db_connection)
    app.cli.add_command(init_db)
