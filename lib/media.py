import os

from flask import Blueprint
from flask import current_app
from flask import send_file
from flask import render_template
from flask import send_from_directory


bp = Blueprint("media", __name__, url_prefix="/media")


@bp.route("/<path:filename>")
def static(filename):
    """ Return static product png image file from data/media 
        and dummy 'not_found.png' image from static/img folder
        if original file not found """
    path = os.path.join(current_app.root_path, "..", current_app.config["MEDIA"], filename)

    if os.path.isfile(path) is True:
        return send_file(path, mimetype="image/png")        
    else:
        return send_from_directory('static', 'img/not_found.png', mimetype="image/png")
