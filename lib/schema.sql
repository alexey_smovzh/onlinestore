DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS ingredient;
DROP TABLE IF EXISTS composition;
DROP TABLE IF EXISTS media;
DROP TABLE IF EXISTS unit;



-- List of ingredients 
CREATE TABLE ingredient (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    -- each ingredient should be placed only once in the list
    UNIQUE(name)
);


-- Product composion of ingredients
CREATE TABLE composition (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    product_id INTEGER NOT NULL,
    ingredient_id INTEGER NOT NULL,
    -- it could be 1 kg, 1 litre, 1 piece etc.
    qty TEXT NOT NULL,
    -- Constraint to restric deletion of ingredient if it still used in some composition
    FOREIGN KEY (ingredient_id) REFERENCES ingredient(id)
);
-- this columns are used in SELECT statements in WHERE condition
CREATE INDEX comproindex ON composition(product_id);
CREATE INDEX comingindex ON composition(ingredient_id);


-- Measurement units list --
CREATE TABLE unit (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    -- each unit should be only in sigle copy
    UNIQUE(name)
);


-- Products list
CREATE TABLE product (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    qty INTEGER NOT NULL,
    price INTEGER NOT NULL,
    unit_id INTEGER NOT NULL,
    -- laconic and full description
    laconic TEXT,
    full TEXT,
    -- Constraint to restric deletion of unit if it still used in some product
    FOREIGN KEY (unit_id) REFERENCES unit(id)
);


-- Products photo and video files
CREATE TABLE media (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    product_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    -- media type 
    --      'p' - photo
    --      'v' - video
    type TEXT NOT NULL,
    -- constraint to restrict deletion of media if it used in some product description
    FOREIGN KEY (product_id) REFERENCES product(id)
);
-- this column are used in SELECT statements in WHERE condition
CREATE INDEX mediaprodindex ON media(product_id);


--
-- todo: test data remove in production
--
INSERT INTO unit VALUES(1, '1шт');
INSERT INTO unit VALUES(2, '0.5кг');

INSERT INTO ingredient VALUES(1,'Свинина');
INSERT INTO ingredient VALUES(2,'Говядина');
INSERT INTO ingredient VALUES(3,'Черный перец');
INSERT INTO ingredient VALUES(4,'Соль');
INSERT INTO ingredient VALUES(5,'Мускатный орех');
INSERT INTO ingredient VALUES(6,'Свиной пузырь');
INSERT INTO ingredient VALUES(7,'Натуральная оболочка');
INSERT INTO ingredient VALUES(8,'Коллагеновая оболочка');
INSERT INTO ingredient VALUES(9,'Стартовая культура');
INSERT INTO ingredient VALUES(10,'Пеницилиновая плесень');
INSERT INTO ingredient VALUES(11,'Кьянти');
INSERT INTO ingredient VALUES(12,'Высушенный чеснок');
INSERT INTO ingredient VALUES(13,'Красный перец');


INSERT INTO composition VALUES(11,8,1,'1кг');
INSERT INTO composition VALUES(12,8,3,'50г');
INSERT INTO composition VALUES(13,8,4,'50г');
INSERT INTO composition VALUES(14,8,7,'1шт');
INSERT INTO composition VALUES(15,8,9,'10г');
INSERT INTO composition VALUES(16,8,10,'10г');
INSERT INTO composition VALUES(17,8,11,'100мл');
INSERT INTO composition VALUES(18,9,1,'1кг');
INSERT INTO composition VALUES(19,9,3,'50г');
INSERT INTO composition VALUES(20,9,12,'20г');
INSERT INTO composition VALUES(21,9,4,'20г');
INSERT INTO composition VALUES(22,9,7,'1шт');
INSERT INTO composition VALUES(23,9,9,'10г');
INSERT INTO composition VALUES(24,10,1,'1кг');
INSERT INTO composition VALUES(25,10,3,'50г');
INSERT INTO composition VALUES(26,10,4,'50г');
INSERT INTO composition VALUES(27,10,5,'10г');
INSERT INTO composition VALUES(28,10,9,'10г');
INSERT INTO composition VALUES(29,10,7,'1шт');
INSERT INTO composition VALUES(30,11,1,'1кг');
INSERT INTO composition VALUES(31,11,3,'50г');
INSERT INTO composition VALUES(32,11,4,'50г');
INSERT INTO composition VALUES(33,11,13,'20г');
INSERT INTO composition VALUES(34,11,12,'10г');
INSERT INTO composition VALUES(35,11,7,'1шт');
INSERT INTO composition VALUES(36,12,1,'500г');
INSERT INTO composition VALUES(37,12,3,'20г');
INSERT INTO composition VALUES(38,12,11,'100мл');
INSERT INTO composition VALUES(39,12,4,'20г');
INSERT INTO composition VALUES(40,12,7,'1шт');


INSERT INTO product VALUES(8,'Феллино Салями',8,180,1,'Феллино салями, также известная как "король салями", происходит из небольшого городка в Италии - Феллино.',replace(replace('Эта салями широко известна благодаря своей мягкой текстуре и яркому вкусу, богатому черным перцем и вином.\r\n<p>\r\nЭта деликатная салями содержит незначительное количество других специй и выдержана для получения приятного вкуса.\r\n<p>\r\nНа срезе она ярко красная с мягкой свининой перекрученной на мясорубке и кусочками сала разбросаны маленькими вкраплениями по поверхности.\r\n<p>\r\nПри подаче мы рекомендуем нарезать салями кусочками толщиной в 5 миллиметров. Это гарантирует великолепный баланс между ярким вкусом мяса и черного перца.','\r',char(13)),'\n',char(10)));
INSERT INTO product VALUES(9,'Сопрессата Салями',14,120,1,'Сопрессата была придумана в Италии и сейчас пользуется большой популярностью в Соединенных Штатах.',replace(replace('Сопрессата это простая салями, вкус которой сформирован двумя специями. Молотым черный перец предает ей приятный вкус и покрошенный Калабрийский красный перец придает остроту.\r\n<p>\r\nСвинину измельчают на мясорубке с этими двумя перцами в пропорциях в зависимости от региона и принятых в нем традиций. После чего фарш набивают в натуральную оболочку и подвешивают для вызревания.\r\n<p>\r\nВ результате салями приобретает мягкую текстуру и выдающийся насыщенный вкус.','\r',char(13)),'\n',char(10)));
INSERT INTO product VALUES(10,'Чоризо Салями',4,240,1,'Чоризо валенная салями в Испанском стиле. Богатый вкус копченой паприки, свежего чеснока, пряностей.',replace(replace('Важно обратить внимание на то, что существуют два наиболее распространенных вида Чоризо, Мексиканское и Испанское.\r\n<p>\r\nМексиканское Чоризо готовят из перекрученной свинины и продают свежей, не приготовленной.\r\n<p>\r\nИспанская Чоризо также состоит из перекрученной свинины со специями, но вместо продажи в сыром виде, ее валят.\r\n<p>\r\nВ результате этого Испанская Чоризо, единственная чоризо признаваемая одним из подвидов салями.\r\n<p>\r\nОна является превосходной альтернативой Пепперони, если вы любите более острый и более насыщенный специями вкус. \r\n<p>\r\nЧоризо это мечта любителя специй с насыщенным вкусом и глубокими копчеными полутонами.','\r',char(13)),'\n',char(10)));
INSERT INTO product VALUES(11,'Финночиона Салями',6,160,1,'Финночиона Салями это вид салями который произошел из Тосканы сотни лет назад.',replace(replace('Согласно местной истории, черный перец, важный ингредиент в салями был настолько дорогим, что вместо него местные жители использовали специю широко распространенную в этой местности - фенхель.\r\n<p>\r\n"Фенночио" по итальянски означает фенхель.\r\n<p>\r\nВкус салями Фенночиона богат смесью пряностей, включая поджаренные семена фенхеля. Вместе они создают продукт обладающий сильным приятным ароматом.','\r',char(13)),'\n',char(10)));
INSERT INTO product VALUES(12,'Качиаторе Салями',7,80,1,'Качиаторе салями, это салями в охотничьем стиле, получившее свое имя благодаря Итальянским охотникам, которые предпочитали их из-за небольшого размера и вкуса, а также благодаря тому что они являются отличным, питательным перекусом.','Рецепт Качиаторе салями прекрасно простой. Он обеспечивает четкий вкус мяса улучшенный благодаря минимальному количеству специй. С легкими нотками чеснока и меленого черного перца.');


INSERT INTO media VALUES(4,4,'4_0.png','p');
INSERT INTO media VALUES(5,4,'4_1.png','p');
INSERT INTO media VALUES(6,4,'4_2.png','p');
INSERT INTO media VALUES(14,8,'8_0.png','p');
INSERT INTO media VALUES(15,8,'8_1.png','p');
INSERT INTO media VALUES(16,8,'8_2.png','p');
INSERT INTO media VALUES(17,9,'9_0.png','p');
INSERT INTO media VALUES(18,9,'9_1.png','p');
INSERT INTO media VALUES(19,9,'9_2.png','p');
INSERT INTO media VALUES(20,9,'9_3.png','p');
INSERT INTO media VALUES(21,10,'10_0.png','p');
INSERT INTO media VALUES(22,10,'10_1.png','p');
INSERT INTO media VALUES(23,10,'10_2.png','p');
INSERT INTO media VALUES(24,10,'10_3.png','p');
INSERT INTO media VALUES(25,11,'11_0.png','p');
INSERT INTO media VALUES(26,11,'11_1.png','p');
INSERT INTO media VALUES(27,12,'12_0.png','p');
