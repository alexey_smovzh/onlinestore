import os

from flask import Flask
from flask import redirect
from flask import url_for

from onlinestore import product
from onlinestore import cart
from lib import media


DATA=os.path.join("data")


def create_app(test_config=None):
    app = Flask(__name__)
    app.config.update(
        # Data folder
        DATA=DATA,
        # Sqlite3 database file location
        DATABASE=os.path.join(DATA, "database.db"),
        # Image upload path
        MEDIA=os.path.join(DATA, "media"),
    )


    # Register route handlers
    app.register_blueprint(product.bp)
    app.register_blueprint(cart.bp)
    app.register_blueprint(media.bp)


    # Products are index page
    @app.route("/")
    def index():
        return redirect(url_for("product.all"))


    return app
