import json

from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

from lib.database import query_db


bp = Blueprint("cart", __name__, url_prefix="/cart")


@bp.route("/")
def content():
    """ Display shopping cart """
    return render_template("cart.html")



@bp.route("/pay", methods=("POST",))
def pay_post():
    """ Get order data from cart.js and put it in global variable """
    global order
    global total
    order = json.loads(request.form["order"])
    total = request.form["total"]

    return redirect(url_for("cart.pay_get"))



@bp.route("/pay")
def pay_get():
    """ Display payment page """
    return render_template("pay.html", order=order, total=total)



@bp.route("/done", )    
def done():
    """ Final actions when customer completes purchase """
    # update product qty in database
    for product in order:
        name = product['name']
        qty = product['qty']

        result = query_db("UPDATE product \
                           SET qty=(SELECT qty - ? FROM product WHERE name=?) \
                           WHERE name=?", [qty, name, name])
    
        if result is not None:
            return render_template("error.html", error=result)


    # todo: send order details email to customer
    # todo: send order details email to admin

    return redirect(url_for("product.all"))