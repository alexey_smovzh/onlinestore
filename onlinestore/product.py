from flask import Blueprint
from flask import render_template

from lib.database import query_db
from lib.database import select_query_db


bp = Blueprint("product", __name__, url_prefix="/product")


@bp.route("/all/")
def all():
    """ Display all products list """
    status, result = select_query_db("SELECT product.id, product.name, product.qty, product.price, unit.name as unit, product.laconic, media.name as image \
                                      FROM product, unit, media \
                                      WHERE product.id = media.product_id \
                                      AND product.price > 0 \
                                      AND product.unit_id = unit.id \
                                      AND media.id = (SELECT MIN(id) \
                                                      FROM media \
                                                      WHERE media.product_id = product.id)", fetchall=True)
    if status is True:
        return render_template("all.html", products=result)

    return render_template("error.html", error=result)



@bp.route("/<string:product>/<int:id>")
def single(product, id):
    """ Display detailed information about particular product """
    status, composition = select_query_db("SELECT ingredient.name, composition.qty \
                                           FROM ingredient, composition \
                                           WHERE ingredient.id = composition.ingredient_id \
                                           AND composition.product_id=?", [id], fetchall=True)
    if status is False:
        render_template("error.html", error=composition)

    status, product = select_query_db("SELECT product.id, product.name, product.qty, product.full, product.price, unit.name as unit \
                                       FROM product, unit \
                                       WHERE product.unit_id = unit.id \
                                       AND product.id=?", [id])
    if status is False:
        render_template("error.html", error=composition)

    status, media = select_query_db("SELECT name FROM media WHERE type='p' AND product_id=?", [id], fetchall=True)
    if status is False:
        render_template("error.html", error=media)

    # jinja can't access List element by index
    # convert List into Array
    arr = [None] * 4
    idx = 0
    try:
        for row in media:
            arr[idx] = row['name']
            idx += 1
    except IndexError:
        return render_template("error.html", error="Select from table 'media' return more than 4 rows")


    return render_template('product.html', composition=composition, product=product, media=arr)

