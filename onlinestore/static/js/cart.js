// Cart content
cart = [];
// Cart element in navigation bar
let cartQty = document.getElementById("cart_qty");

    
// Constructor
function Item(name, price, qty, max, thumb) {
    this.name = name;
    this.price = price;
    this.qty = qty;
    this.max = max;
    this.thumb = thumb;
}


// Save cart[] content in browser local storage
function saveCart() {
    localStorage.setItem('shoppingCart', JSON.stringify(cart));
}

// Load cart[] from local storage
function loadCart() {
    if(localStorage.getItem('shoppingCart') != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        updateCartQty();
    }
}

// Return total products counts in cart
function totalQty() {
    var totalQty = 0;
    for(var item in cart) {
        totalQty += Number(cart[item].qty);
    }
    return totalQty;
}

// Update cart product count in navigation bar
function updateCartQty() {
    var qty = totalQty();
    if(Number(qty) == 0) {
        cartQty.textContent="Корзина";    
    } else {
        cartQty.textContent="Корзина(" + totalQty() + ")";
    }
}

// Add item to cart
function addItemToCart(name, price, qty, max, thumb) {
    for(var item in cart) {
        if(cart[item].name === name) {
            var sum = Number(cart[item].qty) + Number(qty);
            cart[item].qty = sum;
            updateCartQty();
            saveCart();
            return;
        }
    }
    var item = new Item(name, price, qty, max, thumb);
    cart.push(item);

    updateCartQty();
    saveCart();
}

// Clear cart
function clearCart() {
    cart = [];
    saveCart();
}

// Called when payment system confirms money trasfer
function purchaseDone() {
    clearCart();
    window.location.href = "/cart/done";
}

// Triggered by 'add to cart' button
function addToCart(id) {
    var name = document.getElementById(id + "_name").value;
    var price = document.getElementById(id + "_price").value;
    var qty = document.getElementById(id + "_qty").value;
    var max = document.getElementById(id + "_max").value;
    var thumb = document.getElementById(id + "_thumb").value;

    addItemToCart(name, price, qty, max, thumb);
}

// Display cart content
function displayCart() {
    loadCart();

    var table = document.getElementById("order_table");
    table.innerHTML = "";
    
    // if cart is empty display message that is empty
    if(cart.length === 0) {
        var wrapper = document.getElementById("wrapper");
        table.insertRow(0).outerHTML="<tr><td><h3>Ваша корзина пуста.</h3></td></tr>";
        table.insertRow(1).outerHTML="<tr><td>Вернитесь пожалуйста на главную страницу, что бы добавить в корзину вкусную ветчину, салями или копчености.</td></tr>";        
    } else {    
    // display content of the cart
        var length = 1;
        var total = 0;

        table.insertRow(length-1).outerHTML="<tr><th></th><th align='left'>Товар</th><th align='right'>Количество</th><th align='right'>Цена</th><th align='right'>Стоимость</th><th align='right'>Удалить</th></tr>";
        for(var item in cart) {
            var sum = Number(cart[item].qty) * Number(cart[item].price);
            table.insertRow(length).outerHTML="<tr><td><img class='cart_thumbs' src='"+ cart[item].thumb +"'></td>"
                                            +"<td>"+ cart[item].name +"</td>"
                                            +"<td align='right'>"
                                            +"<button type='button' class='control' onclick='qtyMinusCart("+ item +")'>-</button>"
                                            +"<input type='number' id='"+ item +"_qty' value='"+ cart[item].qty +"' style='width: 3em;'>"
                                            +"<button id='"+ item +"_plus' type='button' class='control' onclick='qtyPlusCart("+ item +", "+ cart[item].max +")'>+</button></td>"
                                            +"<td align='right'>"+ cart[item].price +"</td>"
                                            +"<td align='right'><span id='"+ item +"_sum'>"+ sum +"</span></td>"
                                            +"<td align='right'><button class='control' onclick='removeItemFromCart("+ item +")'>&times;</button></td></tr>";
            length++;
            total += sum;
        }
        table.insertRow(length).outerHTML="<tr><td colspan='6' align='right'><h3>Итого: <span id='total_ui'>"+ total +"</span></h3></td></tr>";
        table.insertRow(length+1).outerHTML="<tr><td colspan='6' align='right'><button type='submit' class='button'>Оформить Заказ</button></td></tr>";
        table.insertRow(length+2).outerHTML="<tr><td><input type='hidden' name='order' id='order' value='"+ JSON.stringify(cart) +"'></td><td><input type='hidden' name='total' id='total' value='"+ total +"'></td></tr>"
    }
}

// Remove item from cart
function removeItemFromCart(id) {
    cart.splice(id, 1);
    saveCart();
    updateCartQty();        
    displayCart();
}

// Change product qty, save result in local storage and update cart table UI
function qtyPlusCart(id, max) {
    if(cart[id].qty < max) {
        cart[id].qty = Number(cart[id].qty) + 1;
        saveCart();
        updateCartQty();        
        displayCart();
    }
}

function qtyMinusCart(id) {
    if(cart[id].qty > 1) {        
        cart[id].qty = Number(cart[id].qty) - 1;
        saveCart();
        updateCartQty();
        displayCart();
    }
}

// Update product qty input type number in UI only
function qtyPlus(id, max) {
    var number = document.getElementById(id +"_qty");
    var button = document.getElementById(id +"_plus");
    if(number.value < max) {
        number.value = Number(number.value) + 1;
    } else {
        button.disabled = true;
    }
}

function qtyMinus(id) {
    var number = document.getElementById(id +"_qty");
    var button = document.getElementById(id +"_plus");
    if(number.value > 1) {
        number.value = Number(number.value) - 1;
        if(button.disabled === true) {
            button.disabled = false;
        }
    }
}