var slideIndex = 1;
var modal = document.getElementById("modal")
function openModal() {  modal.style.display = "block";   }
function closeModal() { modal.style.display = "none";    }
function plusSlides(n) {    showSlides(slideIndex += n); }
function currentSlide(n) {  showSlides(slideIndex = n);  }

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

showSlides(slideIndex);
function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slides");
  var dots = document.getElementsByClassName("thumbs");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
